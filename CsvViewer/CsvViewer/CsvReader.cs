﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvViewer
{
  class CsvReader
  {

    public async Task<List<DataOne>> ReadAsync(object csvPathObj)
    {
      var csvPath = csvPathObj.ToString();
      var result = new List<DataOne>();

      using (var csvFile = new StreamReader(csvPath))
      {
        await csvFile.ReadLineAsync();
        while (!csvFile.EndOfStream)
        {
          var line = await csvFile.ReadLineAsync();
          var splitedLine = line.Split(';');
          var dataOne = new DataOne(
              splitedLine[0],
              splitedLine[1],
              splitedLine[2],
              splitedLine[3],
              splitedLine[4],
              splitedLine[5],
              splitedLine[6],
              int.Parse(splitedLine[7]),
              double.Parse(splitedLine[8], CultureInfo.InvariantCulture),
              double.Parse(splitedLine[9], CultureInfo.InvariantCulture),
              double.Parse(splitedLine[10], CultureInfo.InvariantCulture),
              double.Parse(splitedLine[11], CultureInfo.InvariantCulture)
              );

          result.Add(dataOne);
        }
      }

      return result;
    }


  }
}
