﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CsvViewer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<DataOne> dataOnes;
        List<DataOne> result = null;
        bool isBusy = false;

        public static readonly ICommand OpenCommand = new RoutedCommand("Открыть", typeof(MainWindow));
        private async void OpenExecuted(object sender, RoutedEventArgs e)
        {
            isBusy = true;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Comma separated view|*.csv;";
            if ((bool)dlg.ShowDialog())
            {
                var filePath = dlg.FileName;
                dataOnes.Clear();

                var threadParameters = new ParameterizedThreadStart(obj =>
                {
                    var reader = new CsvReader();
                    var readTask = reader.ReadAsync(obj);
                    result = readTask.Result;
                });

                var newThread = new Thread(threadParameters);
                newThread.Name = "New Task Thread";
                result = null;
                newThread.Start(filePath);
                await Task.Run(() =>
                {
                    while (result == null)
                    {
                        Task.Delay(100);
                    }

                });

                foreach (var item in result)
                {
                    dataOnes.Add(item);
                    await Task.Delay(1);
                }

                isBusy = false;
            }
        }

        public static readonly ICommand SaveCommand = new RoutedCommand("Сохранить", typeof(MainWindow));
        private async void SaveExecuted(object sender, RoutedEventArgs e)
        {
            if (!isBusy)
            {
                var bdWorker = new WorkerDB();
              int errorCode = await bdWorker.WriteDataAsync(dataOnes);
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            dataOnes = new ObservableCollection<DataOne>()
            {
                new DataOne("01.01.2018", "B", "Type1", "C", "Type2","in","#00008B",5,60.023898,30.406769,60.015158,30.391641),
                new DataOne("01.01.2018", "A", "Type1", "B", "Type1","out", "red", 2,60.012746,30.394991,60.018848,30.416448),
                new DataOne( "02.01.2018", "A", "Type1", "B", "Type1","out", "black", 3, 60.018848, 30.416448, 60.023898, 30.406769)
            };

            this.dgCsvView.ItemsSource = dataOnes;
        }


    }
}
