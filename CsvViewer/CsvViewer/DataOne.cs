﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CsvViewer
{
    public enum Direction { @in, @out };
    class DataOne
    {
        DateTime date;
        string objectA;
        string typeA;
        string objectB;
        string typeB;
        Direction direction;
        Color color;
        int intensity;
        double latitudeA;
        double longitudeA;
        double latitudeB;
        double longitudeB;

        public DateTime Date { get => date; set => date = value; }
        public string ObjectA { get => objectA; set => objectA = value; }
        public string TypeA { get => typeA; set => typeA = value; }
        public string ObjectB { get => objectB; set => objectB = value; }
        public string TypeB { get => typeB; set => typeB = value; }
        public Color Colour { get => color; set => color = value; }
        public Direction Direction { get => direction; set => direction = value; }
        public int Intensity { get => intensity; set => intensity = value; }
        public double LatitudeA { get => latitudeA; set => latitudeA = value; }
        public double LongitudeA { get => longitudeA; set => longitudeA = value; }
        public double LatitudeB { get => latitudeB; set => latitudeB = value; }
        public double LongitudeB { get => longitudeB; set => longitudeB = value; }

        public DataOne(
            string date,
            string objectA,
            string typeA,
            string objectB,
            string typeB,
            string direction,
            string color,
            int intensity,
            double latitudeA,
            double longitudeA,
            double latitudeB,
            double longitudeB)
        {
            this.date = DateTime.Parse(date);
            this.objectA = objectA;
            this.typeA = typeA;
            this.objectB = objectB;
            this.typeB = typeB;
            this.direction = (Direction)Enum.Parse(typeof(Direction), direction);
            this.color = (Color)ColorConverter.ConvertFromString(color);
            this.intensity = intensity;
            this.latitudeA = latitudeA;
            this.longitudeA = longitudeA;
            this.latitudeB = latitudeB;
            this.longitudeB = longitudeB;

        }
    }
}
