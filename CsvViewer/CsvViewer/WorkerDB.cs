﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvViewer
{
    class WorkerDB
    {       
        public async Task<int> WriteDataAsync(IEnumerable<DataOne> dataOnes)
        {
            int result = -1;
            var dataOnesCopy = new List<DataOne>(dataOnes);

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "localhost";
            builder.UserID = "sa";
            builder.Password = "!Q2w3e4r+";
            builder.InitialCatalog = "master";

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();

                string sql = $"if not exists " +
                    $"(select * from sys.tables where name=\'{nameof(dataOnes)}\') " +
                    $"create table {nameof(dataOnes)} (" +
                    $"id int PRIMARY KEY IDENTITY" +
                    $"{nameof(DataOne.Date)} datetime not null" +
                    $"{nameof(DataOne.ObjectA)} varchar(64) not null" +
                    $"{nameof(DataOne.TypeA)} varchar(64) not null" +
                    $"{nameof(DataOne.ObjectB)} varchar(64) not null" +
                    $"{nameof(DataOne.TypeB)} varchar(64) not null" +
                    $"{nameof(DataOne.Direction)} int not null" +
                    $"{nameof(DataOne.Colour)} int not null" +
                    $"{nameof(DataOne.Intensity)} int not null" +
                    $"{nameof(DataOne.LatitudeA)} real not null" +
                    $"{nameof(DataOne.LongitudeA)} real not null" +
                    $"{nameof(DataOne.LatitudeB)} real not null" +
                    $"{nameof(DataOne.LongitudeB)} real not null" +
                    $");";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.ExecuteNonQuery();                    
                }

                foreach (var one in dataOnesCopy)
                {
                    sql = $"INSERT INTO {nameof(dataOnes)} (" +
                    $"{nameof(DataOne.Date)}, " +
                    $"{nameof(DataOne.ObjectA)}, " +
                    $"{nameof(DataOne.TypeA)}, " +
                    $"{nameof(DataOne.ObjectB)}, " +
                    $"{nameof(DataOne.TypeB)}, " +
                    $"{nameof(DataOne.Direction)}, " +
                    $"{nameof(DataOne.Colour)}, " +
                    $"{nameof(DataOne.Intensity)}, " +
                    $"{nameof(DataOne.LatitudeA)}, " +
                    $"{nameof(DataOne.LongitudeA)}, " +
                    $"{nameof(DataOne.LatitudeB)}, " +
                    $"{nameof(DataOne.LongitudeB)}, " +
                    $") VALUES (" +
                    $"'{one.Date.ToString("s")}', " +
                    $"'{one.ObjectA}', " +
                    $"'{one.TypeA}', " +
                    $"'{one.ObjectB}', " +
                    $"'{one.TypeB}', " +
                    $"'{one.Direction}', " +
                    $"'{one.Colour}', " +
                    $"'{one.Intensity}', " +
                    $"'{one.LatitudeA}', " +
                    $"'{one.LongitudeA}', " +
                    $"'{one.LatitudeB}', " +
                    $"'{one.LongitudeB}'" +
                    $")";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        result = await command.ExecuteNonQueryAsync();
                    }

                    //https://sqlchoice.azurewebsites.net/en-us/sql-server/developer-get-started/csharp/win/step/2.html
                    //https://stackoverflow.com/a/20876320
                    //https://www.promotic.eu/en/pmdoc/Subsystems/Db/MsSQL/DataTypes.htm

                }


            }

            return result;
        }
    }
}
